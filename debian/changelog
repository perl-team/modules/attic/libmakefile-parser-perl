libmakefile-parser-perl (0.216-1) UNRELEASED; urgency=medium

  NOTE:
  0.216 still FTBFS. #749357

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Mikhail Gusarov ]
  * Remove myself from Uploaders.

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 0.216

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

 -- gregor herrmann <gregoa@debian.org>  Sat, 23 May 2015 17:10:31 +0200

libmakefile-parser-perl (0.215-2) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Add patch to fix POD issues. (Closes: #711576)
  * debian/copyright: switch formatting to Copyright-Format 1.0.
  * Set Standards-Version to 3.9.4 (no further changes).

 -- gregor herrmann <gregoa@debian.org>  Sat, 08 Jun 2013 02:27:42 +0200

libmakefile-parser-perl (0.215-1) unstable; urgency=low

  [ Harlan Lieberman-Berg ]
  * Team upload.
  * New upstream version (0.215).
  * Bump compat, debhelper to 8.  Bump s-v to 3.9.2.
  * Cleanup d/copyright file with minor compliance tweaks.
  * Removed spelling patch which was accepted upstream.
  * Added patch to fix minor misspellings in the Makefile::Parser
    manpage.
  * Correct misspellings in Makefile::Parser::GMakeDB manpage as well.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Email change: Salvatore Bonaccorso -> carnil@debian.org
  * Refresh copyright information for included Test::Base
  * Add copyright information for included Test::Builder
  * Add copyright information for included Test::More
  * Update copyright years for included Module::Install
  * Update copyright years for upstream files
  * Update copyright years for debian/* packaging
  * Refer to Debian systems in general.
    Don't refer of only Debian GNU/Linux systems.

 -- Harlan Lieberman-Berg <H.LiebermanBerg@gmail.com>  Fri, 30 Sep 2011 11:28:08 -0400

libmakefile-parser-perl (0.211-2) unstable; urgency=low

  * Add myself to Uploaders.
  * Add libipc-run3-perl to Depends as needed for the makesimple and
    pgmake-db scripts (Closes: #591680).
  * Bump Build-Depends on debhelper to (>= 7.2.13) for AutoInstall.
  * Convert to '3.0 (quilt)' source package format.
  * debian/copyright:
    - Add myself to copyright for debian/* packaging
    - Refresh debian/copyright to revision 135 of DEP5 format-
      specification for machine-readable copyright files.
    - Explicitly point to GPL-1 license text in common-licenses.
  * Bump Standards-Version to 3.9.1.
  * Add fix-spelling-error-in-manpage to fix a small spelling error in
    Makefile::AST manpage.

 -- Salvatore Bonaccorso <salvatore.bonaccorso@gmail.com>  Thu, 05 Aug 2010 09:18:44 +0200

libmakefile-parser-perl (0.211-1) unstable; urgency=low

  * Initial release (Closes: #542916).

 -- Mikhail Gusarov <dottedmag@dottedmag.net>  Sun, 23 Aug 2009 05:44:52 +0700
